#Primera evaluación.

- Primero cree un nuevo repositorio dando click en **New project**,
le puse nombre(primera-ev) e inicialize un README.md (para esto yo ya tenia 
git en mi PC y la configuracion de correo y usuario, ademas de 
contar con la carpeta en donde alojaria mi repositorio).

- Despues clone el repositorio usando 
**git clone https://gitlab.com/elMalDto/primera-ev.git**

- Creo un archivo de texto con:
**nano archivo.txt** y hago el primer commit con:
	- git add .
        - git commit -m "Primer commit"
        - git push origin master

- Creo una carpeta con: ** mkdir carpeta2**,
y añado un archivo de texto con:
**nano archivo.txt** y lo edito.
- Ahora busco hacer el segundo commit y hago lo siguiente:
	- git add .
	- git commit -m "Segundo commit"
	- git push origin master

- En este punto muevo una imagen a la carpeta raíz con:
**mv pica.jpg primera-ev** y procedo:
	- git add .
        - git commit -m "Tercer commit"
        - git push origin master

- Ahora, necesito visualizar mis commits y lo hago con:
**git log**,
me muestra la lista de mis commits y navego al primero usando:
**git checkout <HASH4(los 4 primeros digitos del HASH)>**, y
hago lo mismo para el segundo y el tercero para terminar regresando
al ultimo commit con:
**git checkout master**
